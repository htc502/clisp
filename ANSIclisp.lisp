(defun compress (x)
  (if (consp x)
    (compr (car x) 1 (cdr x))
    x))
(defun compr (elt n lst)
  (if (null lst)
    (list (n-elts elt n))
    (let ((next (car lst)))
      (if (eql next elt)
        (compr elt (+ n 1) (cdr lst))
        (cons (n-elts elt n)
              (compr next 1 (cdr lst)))))))
(defun n-elts (elt n)
  (if (> n 1)
    (list n elt)
    elt))
(defun uncompress (lst)
  (if (null lst)
    nil
    (let ((elt (car lst))
          (rest (uncompress (cdr lst))))
      (if (consp elt)
        (append (apply #'list-of elt)
                rest)
        (cons elt rest)))))
(defun list-of (n elt)
  (if (zerop n)
    nil
    (cons elt (list-of (- n 1) elt))))

;(setf x '(1 1 1 1 1 2 8 5 9 4 6 55 4 6 4 5 4 4 4 4))
;(setf x-comp (compress x))
;(setf x-decomp (uncompress x-comp))
;
;(mapcar #'(lambda(x) (+ x 10)) '(1 2 8 9 4 4))
;(mapcar #'list '(1 2 3) '(a b c d))
;
;;keywords argument
;(member 'b '(a b c) :test #'equal)
;
;;make array
;(setf arr (make-array '(2 3) :initial-element nil))
;(aref arr 0 0)
;(setf (aref arr 0 0) 'b)
;;literally refer to an array
;#2a ((b nil nil) (nil nil nil)) ;2-dimensional array
;;array of 1 dimension is a vector
;(setf vec (make-array 4 :initial-element nil)) ;or(setf vec (vector nil nil nil nil))
;#(nil nil nil nil);literally referring
;(svref vec 0);svref for simple vector refer

;;bin-search
(defun bin-search (obj vec)
  (let ((len (length vec)))
    (and (not (zerop len))
         (finder obj vec 0 (- len 1)))))
(defun finder (obj vec start end)
  (format t "~A~%" (subseq vec start (+ end 1)))
  (let ((range (- end start)))
    (if (zerop range)
      (if (eql obj (aref vec start))
        obj
        nil)
      (let ((mid (+ start (round (/ range 2)))))
        (let ((obj2 (aref vec mid)))
          (if (< obj obj2)
            (finder obj vec start (- mid 1))
            (if (> obj obj2)
              (finder obj vec (+ mid 1) end)
              ;;obj == obj2
              obj)))))))

(bin-search 10 #(0 1 2 3 4 5 6 7 8 9))

;; emacs lisp tutorial from xah Lee

;;printing
(message "hi")
(message "welcome to year %d" 2014)
(message "welcome to star %s" "Mar")
(message "ready to print a list: %S" (list 8 2 3))

;;interger is a number followed by point and ZERO
(floatp 3)
(floatp 3.)
(floatp 3.0) ;;only this one returns T

(string-to-number "3")
(number-to-string 3)

;;true and false 
;;nil is false, empty list is false
(if nil "yes" "no")
(if () "yes" "no")
